/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Mécanisme;

import java.awt.Event;
import java.awt.event.MouseEvent;

/**
 *
 * @author eloistree
 */
public class DoubleClick  {
 static private int timeMax = 300;    // double-click speed in ms
 static private long timeMouseDown=0; // last mouse down time
 static private  int lastX=0,lastY=0;  //  last x and y
/*
 static  public boolean mouseDown(Event event, int x, int y){
    
    
    long currentTime = event.when;
    if ((lastX==x) && (lastY==y) && 
       ((event.when-timeMouseDown) < timeMax)) {
        System.out.println("double click " + currentTime);    
        return false;
    }
    else {
        //single click action could be added here
        System.out.println("simple click " + currentTime);
        timeMouseDown = event.when;
        lastX=x;
        lastY=y;
    }
    return true;
  }
*/
    static  public boolean isDoubleClick(MouseEvent event, int x, int y){
    
      /*
    ** check for double click
    */
        
    long currentTime = event.getWhen();
         
    if (event.getButton()!=event.BUTTON1) {
        return false;}  
          
    
    
    
    
    if ((lastX==x) && (lastY==y) && 
       ((event.getWhen()-timeMouseDown) < timeMax)) {
        System.out.println("double click " + currentTime);    
        return true;
    }
    else {
        //single click action could be added here
        System.out.println("simple click " + currentTime);
        timeMouseDown = event.getWhen();
        lastX=x;
        lastY=y;
    }
    return false;
  }

    public static boolean isDoubleClickDroit(MouseEvent event, int x, int y) {
             /*
    ** check for double click
    */
    
    
    long currentTime = event.getWhen();
    
  if (event.getButton()!=event.BUTTON3) {
        return false;}        
        
    
    
    if ((lastX==x) && (lastY==y) && 
       ((event.getWhen()-timeMouseDown) < timeMax)) {
        System.out.println("double click " + currentTime);    
        return true;
    }
    else {
        //single click action could be added here
        System.out.println("simple click " + currentTime);
        timeMouseDown = event.getWhen();
        lastX=x;
        lastY=y;
    }
    return false;
    }
}