/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * JPanNuage.java
 *
 * Created on 17 oct. 2011, 21:23:32
 */
package AllGUI.composant;



import Mécanisme.DoubleClick;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import nuagequivolenetombepas.ControlerNuage;
import nuagequivolenetombepas.modèle.Grille;
import nuagequivolenetombepas.modèle.MotAffiché;
import org.netbeans.lib.awtextra.AbsoluteLayout;

/**
 *
 * @author eloistree
 */
public class JPanNuage extends javax.swing.JPanel implements Observer {

 
    private ControlerNuage cn;
    private Point pointStart;
    private Point pointMove;
    /** Creates new form JPanNuage */
    
 
    
    public JPanNuage(ControlerNuage cn,Grille grille) {
       this.cn=cn;
       this.setSize(grille.getWidth(), grille.getHeight());
       this.setBackground(grille.getBackGround());
    
    }
  

 
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanCloud = new javax.swing.JPanel();

        jPanCloud.setBackground(new java.awt.Color(255, 255, 255));
        jPanCloud.setPreferredSize(new java.awt.Dimension(200, 200));
        jPanCloud.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanCloudMousePressed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanCloudLayout = new org.jdesktop.layout.GroupLayout(jPanCloud);
        jPanCloud.setLayout(jPanCloudLayout);
        jPanCloudLayout.setHorizontalGroup(
            jPanCloudLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 260, Short.MAX_VALUE)
        );
        jPanCloudLayout.setVerticalGroup(
            jPanCloudLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 180, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanCloud, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 260, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanCloud, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 180, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

private void jPanCloudMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanCloudMousePressed
cn.selectedNuageBackGround();
}//GEN-LAST:event_jPanCloudMousePressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanCloud;
    // End of variables declaration//GEN-END:variables

    
    public void add(final ObjetPositionné value)
    {
       super.add(value);
       
                            value.addMouseWheelListener(cn);
                            value.addMouseListener (new java.awt.event.MouseAdapter() {
         
            @Override
                            public void mouseClicked(java.awt.event.MouseEvent evt)
                            {
                                if(DoubleClick.isDoubleClick(evt, pointStart.x, pointStart.y))
                                {
                                 
                                 cn.setSizeToThisMot(value.getText(), value.getFont().getSize()+10);
                                
                                }
                                if(DoubleClick.isDoubleClickDroit(evt, pointStart.x, pointStart.y))
                                {
                                 
                                 cn.setSizeToThisMot(value.getText(), value.getFont().getSize()-10);
                                
                                }
                                
                                if (evt.getButton()==MouseEvent.BUTTON2)
                                {
                                
                                  cn.setChangeRotationThisMot(value.getText());
                                
                                }
                            }
                     
            @Override
                            public void mousePressed(java.awt.event.MouseEvent evt) {
                               
                                
                                cn.selected( evt,value);
                                pointStart= evt.getLocationOnScreen();
                                pointMove= (Point) pointStart.clone();
                                value.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                                
                                
                                
                            }
            @Override
                            public void mouseReleased(java.awt.event.MouseEvent evt) {
                                
                                
                                 value.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                                
                            }
                        });
                       
                           value.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
                               
            @Override
                            public void mouseDragged(java.awt.event.MouseEvent evt) {
                                 motPousser();
                               if(pointMove!=null){
                                    ObjetPositionné tmp = (ObjetPositionné) evt.getSource();
                                    Point pointArrivée = evt.getLocationOnScreen();


                                    int xloc =tmp.getLocation().x;
                                    int yloc = tmp.getLocation().y;

                                    int xMove= pointArrivée.x-pointMove.x;;
                                    int yMove = pointArrivée.y-pointMove.y;;

                                        xloc += xMove;
                                        yloc += yMove;

                                    tmp.setLocation(new Point(xloc, yloc));

                                    pointMove=pointArrivée;
                                    tmp.repaint();
                            }
                            }
                        });

       
    }
    
    private void motPousser()
    {
    this.setLayout(new AbsoluteLayout());
    }

    @Override
    public void update(Observable o, Object o1) {
        if(o instanceof MotAffiché)
        {
            
            
            
        
        }
        
        
        
    }

    public ArrayList<ObjetPositionné> getObjetPositionnés() {
  
        ArrayList<ObjetPositionné> valeurFinal = new ArrayList<ObjetPositionné>();
        for (Component tmpComp : this.getComponents())
        {
        valeurFinal.add((ObjetPositionné) tmpComp);
        
        }
        return valeurFinal;
    }
}