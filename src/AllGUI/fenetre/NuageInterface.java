/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * NuageInterface.java
 *
 * Created on 17 oct. 2011, 22:18:33
 */
package AllGUI.fenetre;

import AllGUI.composant.JPanNuage;
import AllGUI.composant.JPanSelection;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import nuagequivolenetombepas.Brain;
import nuagequivolenetombepas.ControlerNuage;
import org.netbeans.lib.awtextra.AbsoluteLayout;

/**
 *
 * @author eloistree
 */
public class NuageInterface extends javax.swing.JFrame {

    static private int nombreFenetre;
    private JPanNuage nuage;
    private JPanSelection  selection;
    private JPanel jPanFenetre ;
    private JPanel jPanFenetreSup;
    private ControlerNuage cn;
    
    JPanel global, gauche, droit, central;
    
    
    {
    jPanFenetre = new JPanel();
     jPanFenetreSup = new JPanel();
    }
    
    static 
    {
        nombreFenetre=0;
    }
    public NuageInterface() {
        nombreFenetre++;
        initComponents();
        setLocation(100+(nombreFenetre*25), 100+(nombreFenetre*25));
      
        
      
        
        // my initComponents
        this.setMinimumSize(new Dimension(700,510));
       
        jPanFenetreSup.setLayout(new BorderLayout());
        jPanFenetreSup.add(jPanFenetre,java.awt.BorderLayout.CENTER);
        
        jPanFenetre.setBackground(new java.awt.Color(0, 0, 0));
        jPanFenetre.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(51, 102, 0), new java.awt.Color(0, 255, 0)));
       
 
       
        
        
        
        global = new JPanel();
        global.setLayout(new GridLayout(1,1));
       
        gauche = new JPanel();
     
        gauche.setLayout(new GridLayout(1,3));
        gauche.setMaximumSize(new Dimension(200,30));
        gauche.setMinimumSize(new Dimension(200,30));
        gauche.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Modifier  le nuage", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 2, 10))); 
        
        jPanFenetreSup.add(gauche,java.awt.BorderLayout.SOUTH);
        
        droit = new JPanel();
        droit.setLayout(new FlowLayout(java.awt.FlowLayout.LEFT));
        droit.setBackground(new Color(180,180,180));
        
        
        global.add(droit, java.awt.BorderLayout.SOUTH);
       
        
        for(String tmpT : new String[]{ "ASTUCE:","Click => Selection, ","Double Click => + 10, ","Double Click droit => -10, ","Click central => rotation, ","Scroll => couleur"})
        {
            JLabel tmp =new JLabel(tmpT );
            tmp.setFont(new Font("Arial",0, 11 ));
            if (tmpT.indexOf("ASTUCE")>=0) tmp.setForeground(Color.RED);
            droit.add(tmp);
        }
    }

    public NuageInterface(ControlerNuage cn,JPanNuage nuage, JPanSelection selection) {
        this();
        this.cn = cn;
        this.nuage = nuage;
        this.selection = selection;
        this.setTitle("Nuage n°"+nombreFenetre+" ("+nuage.getWidth()+"px,"+nuage.getHeight()+"px)");
       
        this.add(jPanFenetreSup,BorderLayout.CENTER);       
        this.add(this.selection,BorderLayout.EAST);
        int a = (jPanFenetre.getWidth()/2)-(this.nuage.getWidth()/2);
        int b = (jPanFenetre.getHeight()/2)-(this.nuage.getHeight()/2);
       
        
        jPanFenetre.setLayout(new java.awt.FlowLayout(FlowLayout.CENTER));
       
         jPanFenetre.add(this.nuage);
        jPanFenetre.setLayout(new AbsoluteLayout());  
        nuage.setSize(nuage.getWidth(), nuage.getHeight());
        centrerLeNuage();
         
        JButton tmp = new JButton();
        tmp.setActionCommand("REGENERER");
        tmp.addActionListener(cn);
        tmp.setText("Regénérer");
        gauche.add(tmp);
        
        tmp = new JButton();
        tmp.setActionCommand("AGRANDIRNUAGE:5");
        tmp.addActionListener(cn);
        tmp.setText("Taille Nuage +");
        gauche.add(tmp);
        
        tmp = new JButton();
        tmp.setActionCommand("DIMINUERNUAGE:-5");
        tmp.addActionListener(cn);
        tmp.setText("Taille Nuage -");
        
        gauche.add(tmp);
        
         getContentPane().add(global, java.awt.BorderLayout.SOUTH);
        
        
         jMenuXML.setActionCommand("EXPORTERXML");
        jMenuXML.addActionListener(cn);
         
         jMenuXHTML.setActionCommand("EXPORTERXHTML");
        jMenuXHTML.addActionListener(cn);
         jMenuImage.setActionCommand("EXPORTERIMAGE");
        jMenuImage.addActionListener(cn);
        
         jMenuStructurer.setActionCommand("REGENERERSTRUCTURE");
        jMenuStructurer.addActionListener(cn);
       
        jMenuAlpha.setActionCommand("REGENERERALPHA");
        jMenuAlpha.addActionListener(cn);
        
        
       
       
        for(String tmpPolice : Brain.getNamePolices())
        {
           
            JMenuItem tmpMenu = new JMenuItem();
            
            tmpMenu.setText(tmpPolice);
            tmpMenu.setActionCommand("POLICE:"+tmpPolice);
            tmpMenu.addActionListener(cn);
        
            jMenuPolice.add(tmpMenu);
                  
        }
         for(String tmpGamme : Brain.getNameGammes())
        {
           
            JMenuItem tmpMenu = new JMenuItem();
            
            tmpMenu.setText(tmpGamme);
            tmpMenu.setActionCommand("GAMMME:"+tmpGamme);
            tmpMenu.addActionListener(cn);
        
            jMenuGamme.add(tmpMenu);
                   
        }
        
         
        jMenuTaillePlus10.setActionCommand("AGRANDIRNUAGE:10");
        jMenuTaillePlus10.addActionListener(cn);

         jMenuTaillePlus5.setActionCommand("AGRANDIRNUAGE:5");
        jMenuTaillePlus5.addActionListener(cn);
        jMenuTailleMoins5.setActionCommand("DIMINUERNUAGE:-5");
        jMenuTailleMoins5.addActionListener(cn);

         jMenuTailleMoins10.setActionCommand("DIMINUERNUAGE:-10");
        jMenuTailleMoins10.addActionListener(cn);
        
        
        
        
        
        jPanFenetre.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
               centrerLeNuage();
            }
        });
        
    }

 
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenuXML = new javax.swing.JMenuItem();
        jMenuXHTML = new javax.swing.JMenuItem();
        jMenuImage = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenu6 = new javax.swing.JMenu();
        jMenuStructurer = new javax.swing.JMenuItem();
        jMenuAlpha = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuTaillePlus10 = new javax.swing.JMenuItem();
        jMenuTaillePlus5 = new javax.swing.JMenuItem();
        jMenuTailleMoins5 = new javax.swing.JMenuItem();
        jMenuTailleMoins10 = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        jMenuPolice = new javax.swing.JMenu();
        jMenuGamme = new javax.swing.JMenu();
        jMenuDimension = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jMenu1.setText("File");

        jMenu2.setText("Exporter");

        jMenuXML.setText("to XML");
        jMenu2.add(jMenuXML);

        jMenuXHTML.setText("to XHMTL");
        jMenu2.add(jMenuXHTML);

        jMenuImage.setText("to Image");
        jMenu2.add(jMenuImage);

        jMenu1.add(jMenu2);

        jMenuBar1.add(jMenu1);

        jMenu3.setText("Nuage");

        jMenu6.setText("Regénérer le nuage");

        jMenuStructurer.setText("Structurer");
        jMenu6.add(jMenuStructurer);

        jMenuAlpha.setText("Strt. Alphabétiquement");
        jMenu6.add(jMenuAlpha);

        jMenu3.add(jMenu6);

        jMenu5.setText("AdapterNuage");

        jMenuTaillePlus10.setText("Taille des mots +10");
        jMenu5.add(jMenuTaillePlus10);

        jMenuTaillePlus5.setText("Taille des mots +5");
        jMenu5.add(jMenuTaillePlus5);

        jMenuTailleMoins5.setText("Taille des mots -5");
        jMenu5.add(jMenuTailleMoins5);

        jMenuTailleMoins10.setText("Taille des mots -10");
        jMenu5.add(jMenuTailleMoins10);

        jMenu3.add(jMenu5);

        jMenuBar1.add(jMenu3);

        jMenu7.setText("Préférence");

        jMenuPolice.setText("Police");
        jMenu7.add(jMenuPolice);

        jMenuGamme.setText("Gamme");
        jMenu7.add(jMenuGamme);

        jMenuDimension.setText("Dimension");
        jMenuDimension.setEnabled(false);
        jMenu7.add(jMenuDimension);

        jMenuBar1.add(jMenu7);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
nombreFenetre--;
}//GEN-LAST:event_formWindowClosed

private void centrerLeNuage(){
   
    
    
    if(jPanFenetre.getWidth()<nuage.getWidth()){
        
        int newWidth =  nuage.getSize().width+400;
        int newHeight = getSize().height;
        setPreferredSize(new Dimension(newWidth,newHeight));
        setSize(newWidth,newHeight);
    }
    
    if(jPanFenetre.getHeight()<nuage.getHeight()){
        
        int newWidth =  getSize().width;
        int newHeight = nuage.getSize().height+300;
        setPreferredSize(new Dimension(newWidth,newHeight));
        setSize(newWidth,newHeight);
    }
    
    
    nuage.setLocation((jPanFenetre.getWidth()/2)-(nuage.getWidth()/2),(jPanFenetre.getHeight()/2)-(nuage.getHeight()/2));
}
 



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuItem jMenuAlpha;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuDimension;
    private javax.swing.JMenu jMenuGamme;
    private javax.swing.JMenuItem jMenuImage;
    private javax.swing.JMenu jMenuPolice;
    private javax.swing.JMenuItem jMenuStructurer;
    private javax.swing.JMenuItem jMenuTailleMoins10;
    private javax.swing.JMenuItem jMenuTailleMoins5;
    private javax.swing.JMenuItem jMenuTaillePlus10;
    private javax.swing.JMenuItem jMenuTaillePlus5;
    private javax.swing.JMenuItem jMenuXHTML;
    private javax.swing.JMenuItem jMenuXML;
    // End of variables declaration//GEN-END:variables
}
