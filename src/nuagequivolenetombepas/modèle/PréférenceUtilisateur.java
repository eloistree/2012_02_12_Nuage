package nuagequivolenetombepas.modèle;

import AllGUI.composant.Gamme;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author eloistree
 */
public class PréférenceUtilisateur {
    
    
    private boolean motsHorizontal ;
    private boolean caractèreSpéciaux ;
    private String police ;
    
    
    private boolean isAgreeArticle ;
    private boolean isAgreeNombre ;
    private boolean isCouleurSignificative ;
    private boolean isColorTotalyRandom;
    
    private String typeDeStructure ;

 
    
    private int tailleMin;
    private int nbMots;
    private int largeurNuage;
    private int hauteurNuage;
    
    private nuagequivolenetombepas.modèle.Gamme gamme;
    
    
    public PréférenceUtilisateur()
    {
    motsHorizontal = false;
    caractèreSpéciaux = false;
    isAgreeArticle = false;
    isAgreeNombre = false ;
    tailleMin = 3;
    nbMots=20;
    largeurNuage=300;
    hauteurNuage=300;
    isCouleurSignificative=false;
    
    }
    
    
    
    //Accesseur et mutateur

    public boolean isAgreeArticle() {
        return isAgreeArticle;
    }

    public void setAgreeArticle(boolean isAgreeArticle) {
        this.isAgreeArticle = isAgreeArticle;
    }

    public boolean isAgreeNombre() {
        return isAgreeNombre;
    }

    public void setAgreeNombre(boolean isAgreeNombre) {
        this.isAgreeNombre = isAgreeNombre;
    }

    
    
    
    public boolean isCaractèreSpéciaux() {
        return caractèreSpéciaux;
    }

    public void setCaractèreSpéciaux(boolean caractèreSpéciaux) {
        this.caractèreSpéciaux = caractèreSpéciaux;
    }

    public boolean isMotsHorizontal() {
        return motsHorizontal;
    }

    public void setMotsHorizontal(boolean motsHorizontal) {
        this.motsHorizontal = motsHorizontal;
    }

    public int getNbMots() {
        return nbMots;
    }

    public void setNbMots(int nbMots) {
        this.nbMots = nbMots;
    }

    public int getTailleMin() {
        return tailleMin;
    }

    public void setTailleMin(int tailleMin) {
        this.tailleMin = tailleMin;
    }

    public int getHauteurNuage() {
        return hauteurNuage;
    }

    public void setHauteurNuage(int hauteurNuage) {
        this.hauteurNuage = hauteurNuage;
    }

    public int getLargeurNuage() {
        return largeurNuage;
    }

    public void setLargeurNuage(int largeurNuage) {
        this.largeurNuage = largeurNuage;
    }

    public boolean isCouleurSignificative() {
        return isCouleurSignificative;
    }

    public void setCouleurSignificative(boolean isCouleurSignificative) {
        this.isCouleurSignificative = isCouleurSignificative;
    }

    public void setStructureDuNuage(String value) {
        typeDeStructure=value;
    }
      public String getStructureDuNuage() {
        
          return typeDeStructure;
    }

    public String getPolice() {
        return police;
    }

    public void setPolice(String police) {
        this.police = police;
    }

    public nuagequivolenetombepas.modèle.Gamme getGamme() {
        
        
        return gamme;
    }

    public void setGamme(nuagequivolenetombepas.modèle.Gamme value) {
    
    gamme= value;
    
    }

    public boolean isColorTotalyRandom() {
        return isColorTotalyRandom;
    }

    public void setIsColorTotalyRandom(boolean isColorTotalyRandom) {
        this.isColorTotalyRandom = isColorTotalyRandom;
    }

    public PréférenceUtilisateur getCopyOf() {
       
        
        PréférenceUtilisateur tmp = new PréférenceUtilisateur();    
    
     tmp.setMotsHorizontal(motsHorizontal);
     tmp.setPolice(police);      
     tmp.setCaractèreSpéciaux(caractèreSpéciaux);
     tmp.setAgreeNombre(isAgreeNombre);
     tmp.setAgreeArticle(isAgreeArticle);
     tmp.setCouleurSignificative(isCouleurSignificative);
     tmp.setIsColorTotalyRandom(isColorTotalyRandom);
     tmp.setStructureDuNuage(typeDeStructure);
     tmp.setTailleMin(tailleMin);
     tmp.setNbMots(nbMots);
     tmp.setLargeurNuage(largeurNuage);
     tmp.setHauteurNuage(hauteurNuage);
     if (gamme !=null)
     tmp.setGamme(gamme.getCopyOf());
    
     return tmp;
    }
    
    
    
    
}
