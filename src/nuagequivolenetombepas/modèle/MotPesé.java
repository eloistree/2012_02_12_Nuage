/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nuagequivolenetombepas.modèle;

import java.util.Observable;

/**
 *
 * @author eloistree
 */
public class MotPesé extends Observable implements java.lang.Comparable{
    
    
    static  private int NBRMOTPESE = 0;
    
    private String mot;
    private int occurrence;
    /** taux de popularité repésente que vaux ses occurences par rapport au plus petit et au plus grand*/
    private double tauxPopularité;
    
        

    public MotPesé(String string) 
    {
        this.mot = string;
        this.occurrence = 1 ;
        tauxPopularité=0.0;
        incNBRMOTPESE();
    }
    
    public MotPesé(String string, int i) 
    {
        this.mot = string;
        this.occurrence = i ;
        tauxPopularité=0.0;
        incNBRMOTPESE();
    }
    
    
    
    
    
    
    static public int getNBRMOTPESE()
    {
        return NBRMOTPESE;
    }
    
    
     static public void incNBRMOTPESE()
    {
         NBRMOTPESE++;
    }

     
     // accesseur et mutateur
    public String getMot() {
        
      
        return ""+mot;
        
      
      
    }
    

    

    public void setMot(String mot) {
        this.mot = mot;
    }
    
    public void incOccurrence() {
     occurrence++;   
    }

    
    
    
     // accesseur et mutateur
    public int getOccurrence() {
        return occurrence;
    }

    public void setOccurrence(int occurrence) {
        this.occurrence = occurrence;
    }
    
  

    public double getTauxPopularité() {
        return tauxPopularité;
    }
    
    public void setTauxPopularité(double value) {
        
         if (value>=1.0) value=1.0;
         if (value<=0.0) value=0.0;
        tauxPopularité=value;
    }
    
    
    public boolean equals(Object o)
    {
        if (this==o) return true;
        if (o==null) return false;
        if (getClass() != o.getClass()) return false;
        MotPesé tmp = (MotPesé) o;
        if (getMot().equals(tmp.getMot())) return true;
        return false;
        
    }
    public String toString()
    {
    return ""+getMot()+"("+occurrence+","+tauxPopularité+")";
    
    }

        @Override
        public int compareTo(Object o) {

         if (this==o) return 0;
          int a = ((MotPesé) o).getOccurrence(); 
          int b = this.getOccurrence(); 
          if (a > b)  return -1; 
          else if(a == b) return 0; 
          else return 1; 

        }
    
        
      
    
}
