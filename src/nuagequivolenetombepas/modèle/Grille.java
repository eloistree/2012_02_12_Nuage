/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nuagequivolenetombepas.modèle;

import java.awt.Color;
import java.util.Observable;
import java.util.Random;

/**
 *
 * @author eloistree
 */
public class Grille extends Observable{
    static private int maxHeight;
    static private int maxWidth;

    public static void setMaxSize(int i, int i0) {
        maxWidth =i; maxHeight=i0;
    }
    private int height; 
    private int width;
    private Color backGround;
    
   
    static
    {
    maxHeight=800;
    maxWidth=1000;
        
    }
    
    public Grille(int width,int height)
    {
        
    
    this.height = height;
    this.width = width;  
    backGround = new Color(255,255,255);
    }
    
  
    
     private int nombreAléatoireEntreA_B(int aMin,int bMax)
    {
        Random r = new Random();
        return aMin + r.nextInt(bMax - aMin);
       
    }

    static public int getMaxHeight() {
        return maxHeight;
    }

    
    static public int getMaxWidth() {
        return maxWidth;
    }

    
     
    
     
     
 

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        if (height>maxHeight) height=maxHeight;
        if (height<50) height=50;
        this.height = height;
        
        setChanged();
        notifyObservers();
    }

  


    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        if (width>maxWidth) width=maxWidth;
        if (width<50) width=50;
        this.width = width;
        
        setChanged();
        notifyObservers();
    }

    public Color getBackGround() {
        return backGround;
    }

    public void setBackGround(Color backGround) {
        this.backGround = backGround;
    }
     
    
   
     
     public String toString(){
     String tmp ="Dimension: "+ width +" largeur, "+height+" hauteur.";

     return tmp;
     
     }

    public Grille getCopyOf() {
       
        Grille tmp = new Grille(getWidth(),getHeight());
    
        Color tmpColor= new Color(backGround.getRed(),backGround.getGreen(),backGround.getBlue());
        tmp.setBackGround(tmpColor);
        return tmp;
    }


    
}
