/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nuagequivolenetombepas.processus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author eloistree
 */
public class LecteurDeTexte {

    
public static String lecteurDeTexte(File fichier){
     
        String texte="";

    String temp = null;
    FileReader fichierread=null;
    BufferedReader buff=null;

     try{
         fichierread = new FileReader(fichier);
         buff = new BufferedReader(fichierread);
         temp=buff.readLine();
         while(temp!=null)
         {
         texte+=" "+temp;
         temp=buff.readLine();
         }
     }catch(IOException e) {
         System.out.println(e.getMessage());
         System.exit(-1);
         return null;
     }

     return texte;
    }

public static String lecteurDeTexte(String fichier){return lecteurDeTexte(fichier,false);}
public static String lecteurDeTexte(String fichier,boolean withoutSpace){
     
    if (fichier!=null)
    {
        String texte="";

    String temp = null;
    FileReader fichierread=null;
    BufferedReader buff=null;

     try{
         fichierread = new FileReader(fichier);
         buff = new BufferedReader(fichierread);
         temp=buff.readLine();
         while(temp!=null)
         {
         texte+=" "+temp+"\n";
         temp=buff.readLine();
         }
     }catch(IOException e) {
         System.out.println(e.getMessage());
         //System.exit(-1);
         return null;
     }
     
     if (withoutSpace==true)
     {
     while(texte.indexOf("  ")>=0){texte=texte.replace("  "," ");}
     }
    
     return texte;
    }
    return "";
    }
}
