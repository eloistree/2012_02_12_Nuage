/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nuagequivolenetombepas;

import AllGUI.composant.JPanNuage;
import AllGUI.composant.JPanSelection;
import AllGUI.composant.ObjetPositionné;
import AllGUI.fenetre.AfficherResultat;
import AllGUI.fenetre.ChoixCouleur;
import AllGUI.fenetre.NuageInterface;
import Exportation.EnregistrerSous;
import Exportation.Grille2XML;
import Exportation.Transformateur;
import Exportation.WriteInFile;
import java.awt.Color;
import java.awt.FlowLayout;

import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.xml.transform.TransformerException;
import nuagequivolenetombepas.modèle.Gamme;
import nuagequivolenetombepas.modèle.Grille;
import nuagequivolenetombepas.modèle.MotAffiché;
import nuagequivolenetombepas.modèle.PréférenceUtilisateur;
import nuagequivolenetombepas.processus.LecteurDeTexte;

/**
 *
 * @author eloistree
 */
public class ControlerNuage  implements ActionListener,MouseWheelListener{
    
    private NuageInterface vue;
   
    private Grille grille;
    private ArrayList<MotAffiché> motsAffichésOriginelles;
    private ArrayList<MotAffiché> motsAffichésTemporaire;
    private PréférenceUtilisateur préférence;
    
    private MotAffiché motCourant;
  
    private Transformateur transformateur;
    private JPanNuage jPanCloud;
    private JPanSelection jPanSelection;
    
    {
        motsAffichésOriginelles = new ArrayList<MotAffiché>();
         motsAffichésTemporaire = new ArrayList<MotAffiché>();
       //  File xsl =  new File("src/Exportation/transformateurXML2HTML.xsl");
       // if(!xsl.exists())xsl =new File("transformateurXML2HTML.xsl");
        // transformateur= new Transformateur(xsl);
            transformateur= new Transformateur();
       
        
    }

    public ControlerNuage( Grille grille, ArrayList<MotAffiché> motAffiché,PréférenceUtilisateur préféUti) {
      
        this.grille = grille.getCopyOf();
        préférence = préféUti.getCopyOf();
        
        jPanCloud = new JPanNuage(this,grille);
        jPanSelection = new JPanSelection(this);
        
        for(MotAffiché tmp : motAffiché)
      {
         if(tmp!=null)
         {
          System.out.println(tmp.getMot()+", ");
          motsAffichésOriginelles.add(tmp.getACopy());
         }
      }
        
        
        définirJPanCloud();
        
        
        
        jPanCloud.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                selectedNuageBackGround();
            }
        });
        jPanCloud.repaint();
        jPanCloud.validate();
        
        jPanSelection.configurerPanelModifierSelection();
    
        
        
        
    vue = new NuageInterface(this, jPanCloud, jPanSelection);
       
    vue.setVisible(true);
    }
    
    
    
    
    private void définirJPanCloud()
    {
        
        
        jPanCloud.setLayout(new FlowLayout());
                
      System.out.println("Définir les mots:"+motsAffichésOriginelles.size());
      if (motsAffichésTemporaire.size()>0)
      {
          for(MotAffiché tmp: motsAffichésTemporaire)
          {
              tmp.signalerSuppression();
          }
          motsAffichésTemporaire.clear();
      }
      for(MotAffiché tmp : motsAffichésOriginelles)
      {
       System.out.println(tmp.getMot());
      motsAffichésTemporaire.add(tmp.getACopy());
      }
      
     if (préférence!=null)
       
       if (préférence.getStructureDuNuage().equals("structurer") ||préférence.getStructureDuNuage().equals("structurerAlphabétique") )
       {
             if (préférence.getStructureDuNuage().equals("structurerAlphabétique"))
             {
                 System.out.println("tier ...................................");
                         jPanCloud.setLayout(new FlowLayout());
                         Brain.trierAlphabetiquementMotsStocker(motsAffichésTemporaire);

             }
             else
             {
                         jPanCloud.setLayout(new FlowLayout());
                         Brain.shuffleMots(motsAffichésTemporaire);
             }
       }
     
     
     String police = préférence.getPolice();
     
     int i=0;
     
   for(MotAffiché tmp : motsAffichésTemporaire)
      {
          
          
        ObjetPositionné tmpObjectP = new ObjetPositionné();
        
        tmp.setPolice(police);
        définirCouleurDesMots(tmp,préférence);
        
        motsAffichésTemporaire.get(i).addObserver(tmpObjectP);
        jPanCloud.add(tmpObjectP);
        motsAffichésTemporaire.get(i).updateNow();
        System.out.println(i+" mot créer: "+motsAffichésTemporaire.get(i)+"=>"+tmpObjectP.getText());
          i++;
      }
     
    
    
    }
      
   
    
    
     private void définirCouleurDesMots(MotAffiché valueDonnée,PréférenceUtilisateur pref) {
       
         
      
         
         
                      
                                if (pref.isCouleurSignificative())
                                {
                                
                                     java.awt.Color couleur = null;
                                    double populartitéDuMot = valueDonnée.getTauxPopularité();

                                    couleur = new java.awt.Color(
                                            (int) (170.0-170.0*populartitéDuMot),
                                            170+((int) (85.0-85.0*populartitéDuMot)),
                                            (int) (20)
                                            );
                                    
                                    valueDonnée.setCouleur(couleur);
                                    
                                }
                                
                                else if (!pref.isCouleurSignificative() && pref.getGamme()!=null )
                                {
                                     java.awt.Color tmpColor = pref.getGamme().getRandomColor(); 
                                      valueDonnée.setCouleur(tmpColor);
                                    
                                
                                }
                                 else
                                    
                                {
                                    java.awt.Color tmpColor = new java.awt.Color(Brain.nombreAléatoireEntreA_B(0, 255), Brain.nombreAléatoireEntreA_B(0, 255), Brain.nombreAléatoireEntreA_B(0, 255));
                                    valueDonnée.setCouleur(tmpColor);
                                    
                                }
                            
                    

    }
     
    private void setMotCourant(MotAffiché value )
    {   
        if(value !=null)
        {
            if(motCourant!=null)
            {
                this.motCourant.setBorder(false);
                motCourant.deleteObserver(jPanSelection);
            }
            motCourant = value;
            this.motCourant.setBorder(true);
            motCourant.addObserver(jPanSelection);
        }
    
    }
     private void setMotCourant(JPanNuage value )
    {   
        if(value !=null)
        {
            if(motCourant!=null)
            {
                this.motCourant.setBorder(false);
                motCourant.deleteObserver(jPanSelection);
            }
            motCourant = null;
        }
    
    }
     private void setMotCourant()
    {   
        
            if(motCourant!=null)
            {
                this.motCourant.setBorder(false);
                motCourant.deleteObserver(jPanSelection);
            }
            motCourant = null;
        
    
    }
       private void selectedEmpty() {
        setMotCourant();
        jPanSelection.configurerPanelModifierSelection();
       
    }

     public void selected(MouseWheelEvent evt, ObjetPositionné value) {
     
   
      MotAffiché temp= lookFor(value.getText());
      if (temp!=null)
      {
      setMotCourant(temp);
      jPanSelection.configurerPanelModifierSelection(temp);
      }
      else jPanSelection.configurerPanelModifierSelection();
      
    }
    
    public void selected(MouseEvent evt, ObjetPositionné value) {
     
   
      MotAffiché temp= lookFor(value.getText());
      if (temp!=null)
      {
      setMotCourant(temp);
      jPanSelection.configurerPanelModifierSelection(temp);
      }
      else jPanSelection.configurerPanelModifierSelection();
      
    }
    private MotAffiché lookFor(String text) {
        
        System.out.print("Recherche de "+ text+"...");
       
        for(MotAffiché tmp : motsAffichésTemporaire)
        {System.out.println((tmp.getMot()) +"><"+(text));
            if ((tmp.getMot()).equals(text)){ 
        System.out.println("Trouvé ("+tmp.getMot()+")");return tmp;} 
            
        }
        System.out.println("Rien Trouvé");
        return null;
        
    }
    
     public  void  supprimer(String text) {
         
      MotAffiché temp= lookFor(text);
      if (temp!=null)
      {
         
          temp.signalerSuppression();
          motsAffichésTemporaire.remove(temp);
         
          
      } 
      
      
    }
    
      public void selectedNuageBackGround() {
        setMotCourant(jPanCloud);
        jPanSelection.configurerPanelModifierSelection(jPanCloud);
          
          
      }
    
   
    
    public void setColorToThisMot(String value, Color couleur)
    {
        MotAffiché tmp = lookFor(value);
        if (tmp!=null) tmp.setCouleur(couleur);
    }
    
    public void setSizeToThisMot(String value, int  size)
    {
        MotAffiché tmp = lookFor(value);
        if (tmp!=null) tmp.setSize(size);
    }

    public Color ouvrirUnPanelCouleur() {
        
       return ouvrirUnPanelCouleur(Color.WHITE);
    }
    
    public Color ouvrirUnPanelCouleur(Color value) {
        
        ChoixCouleur fenetreTmp = new ChoixCouleur(vue,value);
        Color tmp = fenetreTmp.getCouleur();
        
        fenetreTmp.dispose();
        System.out.println("Couleur choisie:" +tmp);
        return tmp;
    }

    public void setNuageBackGroundTo(Color value) {
        
        if(jPanCloud!=null){
            jPanCloud.setBackground(value);
            grille.setBackGround(value);
            
        }
        
        
    }


    public void setVerticalToThisMot(String text, boolean isVertical) {
        MotAffiché tmp = lookFor(text);
        if (tmp!=null){
            tmp.setIsHorizontal(!isVertical);
            
           
            
        
        }
    }
      public void setChangeRotationThisMot(String text) {
        MotAffiché tmp = lookFor(text);
        if (tmp!=null){
            tmp.setIsHorizontal(!tmp.isHorizontal());
            
           
            
        
        }
    }
      
      public void scrollThisWord(String text)
      {
      
      
      }
      
      


    public void actionPerformed(ActionEvent ae) {
        
        
        
       
        
       
        if (ae.getActionCommand().equals( "REGENERER"))
        {
            System.out.println("REGENERER");
             définirJPanCloud();
        
        }
        else if (ae.getActionCommand().equals( "REGENERERSTRUCTURE"))
        {
            System.out.println("REGENERERSTRUCTURE");
            préférence.setStructureDuNuage("structurer");
            définirJPanCloud();
        
        }else if (ae.getActionCommand().equals( "REGENERERALPHA"))
        {
            System.out.println("REGENERERALPHA");
            préférence.setStructureDuNuage("structurerAlphabétique");
            définirJPanCloud();
        
        }else if (ae.getActionCommand().indexOf( "AGRANDIRNUAGE")>=0)
        {
            
            int value = 2;
            if (ae.getActionCommand().split(":").length>1)
            {String tmp = ae.getActionCommand().split(":")[1];
                try{
                value = Integer.parseInt(tmp);
                }catch (NumberFormatException e){}
            }
            System.out.println("AGRANDIRNUAGE");
            for(MotAffiché tmp: motsAffichésTemporaire)
            {
                tmp.setSize(tmp.getSize()+value);
            }
        
        }else if (ae.getActionCommand().indexOf( "DIMINUERNUAGE")>=0)
        {
             int value = 2;
            if (ae.getActionCommand().split(":").length>1)
            {String tmp = ae.getActionCommand().split(":")[1];
                try{
                value = Integer.parseInt(tmp);
                }catch (NumberFormatException e){}
            }
            
            System.out.println("DIMINUERNUAGE");
            for(MotAffiché tmp: motsAffichésTemporaire)
            {
                tmp.setSize(tmp.getSize()+value);
            }
        
        
        }else if (ae.getActionCommand().equals( "EXPORTERXML"))
        {
            System.out.println("EXPORTERXML");
           
           String tmpA = Grille2XML.getCodeXml(grille,jPanCloud.getObjetPositionnés());
           //System.out.println(tmpA);
           EnregistrerSous tmpSave = new EnregistrerSous(vue,true,"nuageSouriant","xml","Exportation d'un fichier XML du nuage");
           String patchFile=tmpSave.getChemin();
              tmpSave.dispose();
           if( patchFile ==null || patchFile.equals("") || patchFile.indexOf(".xml")<0 )
           {  // il y a un problème
            } 
           else 
           {
               WriteInFile.ecrire(patchFile,tmpA);
              AfficherResultat tmpAffichage= new AfficherResultat(vue,true,tmpA,patchFile);
               tmpAffichage.dispose();
           }
   
        
        }else if (ae.getActionCommand().equals( "EXPORTERXHTML"))
        {
            System.out.println("EXPORTERXHTML");
            
            
            
            String tmpA = Grille2XML.getCodeXml(grille,jPanCloud.getObjetPositionnés());
           //System.out.println(tmpA);
           String patchFileXML="nuageSouriant.xml";
           EnregistrerSous tmpSave = new EnregistrerSous(vue,true,"nuageSouriant","html","Exportation d'un fichier XHTML du nuage");
           String patchFile=tmpSave.getChemin();
           tmpSave.dispose();
           if( patchFile ==null || patchFile.equals("") || patchFile.indexOf(".html")<0 )
           {  // il y a un problème
            } 
           else 
           {
               
                    System.out.println("XML: "+patchFileXML);
                    System.out.println("XHTML: "+patchFile);
           //         System.out.println("XSL: "+transformateur.getPatchXSL());
               
              WriteInFile.ecrire(patchFileXML,tmpA);
                try {
                    
                    transformateur.transforme(patchFileXML,patchFile);
                    tmpA = LecteurDeTexte.lecteurDeTexte(patchFile,true);
                    AfficherResultat tmpAffichage= new AfficherResultat(vue,true,tmpA,patchFile);
                    tmpAffichage.dispose();

                } catch (TransformerException ex) {
                    Logger.getLogger(ControlerNuage.class.getName()).log(Level.SEVERE, null, ex);
                }

                
           }
        
        
        
        }else if (ae.getActionCommand().equals( "EXPORTERIMAGE"))
        {
            selectedEmpty();
            
           EnregistrerSous tmpSave = new EnregistrerSous(vue,true,"saveNuage","png","Exportation d'un fichier PNG du nuage");
           String patchFile=tmpSave.getChemin();
           tmpSave.dispose();
           if( patchFile ==null || patchFile.equals("") || patchFile.indexOf(".png")<0 )
           {  // il y a un problème
            } 
           else 
           {
                    BufferedImage outImage = new BufferedImage(jPanCloud.getWidth(),jPanCloud.getHeight(),BufferedImage.TYPE_INT_RGB);
                    Graphics2D graphics=outImage.createGraphics();
                    jPanCloud.paint(graphics);
                    File outFile=new File(patchFile );
                    try {
                        if (!ImageIO.write(outImage,"png",outFile))
                            System.out.println("Format d'écriture non pris en charge" );
                    } catch (Exception e) {
                        System.out.println("erreur dans l'enregistrement de l'image :" );
                        e.printStackTrace();
                    }
           }
            
        }
        else if ((ae.getActionCommand().indexOf( "POLICE"))>=0)
        { if (ae.getActionCommand().split(":")[1]!=null)
        {String nomPolice = ae.getActionCommand().split(":")[1];
           préférence.setPolice(nomPolice);
           mettreAJourPolice();
        }
        }
        else if ((ae.getActionCommand().indexOf( "GAMMME"))>=0)
        {
            if (ae.getActionCommand().split(":")[1]!=null)
            {
               String nomGamme = ae.getActionCommand().split(":")[1];   
               préférence.setGamme(Brain.getGammes(nomGamme));
               mettreAJourGamme();
               
            }
            
        }
       
        
       
         
        
        
        
        
        
    }

    
    private void mettreAJourPolice() {
        if(préférence !=null  && motsAffichésTemporaire !=null){
        String tmpP = préférence.getPolice();
         for(MotAffiché tmp: motsAffichésTemporaire)
            {
                tmp.setPolice(tmpP);
            }
        }
    }

    private void mettreAJourGamme() {
       if(préférence !=null  && motsAffichésTemporaire !=null){
        Gamme tmpP = préférence.getGamme();
         for(MotAffiché tmp: motsAffichésTemporaire)
            {
                tmp.setCouleur(préférence.getGamme().getRandomColor() );
            }
        }
        
    }

    
    private int dernierScroll =0;
    public void mouseWheelMoved(MouseWheelEvent mwe) {
      if (mwe.getSource() instanceof ObjetPositionné)
      {
          if (mwe.getSource()!=null){
              MotAffiché leMot= lookFor(((ObjetPositionné)mwe.getSource()).getText()); 
              Gamme gamme = préférence.getGamme();
              int min, max;
              
              if (gamme!=null && gamme.getCouleurs()!=null)
              {
                  selected( mwe ,(ObjetPositionné) mwe.getSource());
                  min =0; max =gamme.getCouleurs().length-1 ;

                  int unitéeScollée = mwe.getUnitsToScroll();
                  if(unitéeScollée<0)
                  {
                    dernierScroll--;
                    if (dernierScroll<0) dernierScroll= max;
                  }
                  else if (unitéeScollée>0)
                  {

                     dernierScroll++;
                    if (dernierScroll>max) dernierScroll= 0;

                  }

                  
                  leMot.setCouleur(gamme.getCouleurs()[dernierScroll]);

              }
              else if (préférence.isColorTotalyRandom())
              {
                  
               leMot.setCouleur(new Color(Brain.nombreAléatoireEntreA_B(0,255),Brain.nombreAléatoireEntreA_B(0,255),Brain.nombreAléatoireEntreA_B(0,255)));
              }
          }
      
      }
        
    }

  

  
    

    

  
}
