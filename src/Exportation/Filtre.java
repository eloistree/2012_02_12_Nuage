/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Exportation;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author eloistree
 */
public class Filtre extends FileFilter{
 
    String [] suffixes;
    String descriptions;

    public Filtre (String [] lesSuffixes, String description)
    {
    this.suffixes = lesSuffixes;
    this.descriptions = description;
    
    }
    
    
    boolean appartient(String suffixe)
    {
    
        for (int i =0; i<suffixes.length;++i)
        {
            if (suffixe.equals(suffixes[i]));
            return true;
        }

        return false;
        
    }
    
    
    @Override
    public boolean accept(File file) {
    if(file.isDirectory())return true;
    
    String suffixe=null;
    String s = file.getName();
    
        int i = s.lastIndexOf('.');
        if(i>0 && i< s.length()-1)
            suffixe = s.substring(i+1).toLowerCase();
    return suffixe!=null && appartient(suffixe);
    }

    @Override
    public String getDescription() {
     return descriptions;
    }
    
    
    
}
