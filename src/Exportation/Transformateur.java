/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Exportation;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 *
 * @author eloistree
 */
public class Transformateur {
    
    
      
    private Transformer transformateur;
    private File xsl;

    
    /**
     * Transformateur  permet à partir d'une feuille xsl de transformer un fichier xml en fichier html.
     * 
     */
   public Transformateur(File xslSource) throws TransformerConfigurationException
   {
       xsl=xslSource;

        TransformerFactory transformateurFact = TransformerFactory.newInstance();
     
         transformateur = transformateurFact.newTransformer(new StreamSource(xslSource));
      
      
   }

    public Transformateur() {
        try {
            TransformerFactory transformateurFact = TransformerFactory.newInstance();
         
             transformateur = transformateurFact.newTransformer(new StreamSource(getClass().getResourceAsStream("/elementExterieur/transformateurXML2HTML.xsl")));
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(Transformateur.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }

  

   /**
    * transforme grace à un xslt l'origine xml en fichier html destionation
    * @params origine chemain d'accès vers le fichier xml
    * @params destination chemain d'accès vers le future fichier html
    */
    public boolean transforme(String origine,String destination) throws TransformerException
    {
        File a,b;
        a = new File (origine);
        b = new File (destination);
        
     
         if(transformateur!=null && origine!=null && destination!=null
               && a.exists()  )
         {
            
                 transformateur.transform(new StreamSource(a), new StreamResult(b));
                
                return true;
        }
         return false;
         
    }

    public String getPatchXSL() {
        return this.xsl.getAbsolutePath();
    }
}
